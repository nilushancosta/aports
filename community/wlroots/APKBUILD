# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Contributor: Drew DeVault <sir@cmpwn.com>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=wlroots
pkgver=0.15.0
pkgrel=0
pkgdesc="Modular Wayland compositor library"
url="https://gitlab.freedesktop.org/wlroots/wlroots"
license="MIT"
arch="all"
makedepends="
	eudev-dev
	libcap-dev
	libinput-dev
	libseat-dev
	libxcb-dev
	libxkbcommon-dev
	mesa-dev
	meson
	ninja
	pixman-dev
	wayland-dev
	wayland-protocols
	xcb-util-image-dev
	xcb-util-renderutil-dev
	xcb-util-wm-dev
	xkeyboard-config
	xwayland-dev
	"
subpackages="$pkgname-dbg $pkgname-dev"
source="https://gitlab.freedesktop.org/wlroots/wlroots/-/archive/$pkgver/wlroots-$pkgver.tar.gz"
options="!check" # no test suite

build() {
	abuild-meson \
		-Dexamples=false \
		. build
	meson compile ${JOBS:+-j ${JOBS}} -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build
}

sha512sums="
730b1e0cf003dabae23101664c6d1993636bd3a8ecbdb3c165eef415a92811c4d5228f48e0275f75361d8528f118dfb8a2298cfb05dbf6364539224ceedca447  wlroots-0.15.0.tar.gz
"
