# Contributor: Adrian L Lange <alpine@p3lim.net>
# Contributor: Charles Wimmer <charles@wimmer.net>
# Contributor: Dermot Bradley <dermot_bradley@yahoo.com>
# Maintainer: Dermot Bradley <dermot_bradley@yahoo.com>
pkgname=step-certificates
pkgver=0.18.0
pkgrel=0
pkgdesc="Online certificate authority and related tools"
url="https://smallstep.com/certificates/"
arch="all !s390x !mips !mips64"
license="Apache-2.0"
makedepends="
	bash
	go
	go-bindata
	libcap
	pcsc-lite-dev
	"
depends="step-cli"
options="!check" # No tests as fails for Yubikey
install="$pkgname.pre-install"
subpackages="
	$pkgname-openrc
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/smallstep/certificates/releases/download/v$pkgver/step-ca_$pkgver.tar.gz
	01-Makefile-GOFLAGS.patch
	02-Makefile-yubikey-and-pkcs11-binary.patch
	step-ca.confd
	step-ca.initd
	step-ca.logrotate
	"
builddir="$srcdir"

build() {
	make simple
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	setcap cap_net_bind_service=+ep "$pkgdir"/usr/bin/step-ca

	install -m 644 -D "$srcdir"/step-ca.confd "$pkgdir"/etc/conf.d/step-ca
	install -m 755 -D "$srcdir"/step-ca.initd "$pkgdir"/etc/init.d/step-ca

	install -m 644 -D "$srcdir"/step-ca.logrotate "$pkgdir"/etc/logrotate.d/step-ca
}

sha512sums="
a7c85f999d9f330fe44ee095d2b66e7805ca27cdc02b524bbd9a81f6e0c019762fb497d2c2262b903b825babb2f0df02b0431f4b3cce8fae963b9a1bd85b078b  step-certificates-0.18.0.tar.gz
fce464b646dfb087da27beed7135c52b2926df8595675ab3117afa9a98ac2c8ac8bed57ddf6567bcccc417dc076321a230a77ef029fad2ab3213c2b3655eb66b  01-Makefile-GOFLAGS.patch
0b49bda9b0b0df9d9e10ab96405a8eff5b50aaf354b8ce424d4f085ef7b033d98169521f798b27dce3959ea18cf97da365613417c40a3d54779fad7ecc15e3d4  02-Makefile-yubikey-and-pkcs11-binary.patch
c3a6ac7216643dba6dd54bd709e94e6c2d3ad8c04a342845a8873964e3bbca8f96cf40517628af8a0b8ed861757cf09ccea5a65804ba7849114bc75c3636e5d1  step-ca.confd
9dd2342434f3330d5ce7a6461a71ac1a40f9e1c17f2fc0599f4882c4ba3497ec09809b84f54828a9db87777e65905aff880f36610b3a157c33b6491cbd7ca82b  step-ca.initd
0af3914768275d26cd25a36ac4b33218429da00e3c719bdd7841507b6dc4a9d8c4db30e502072accc111d25333888e9f310baf8260f07545a083ba13719eb4ed  step-ca.logrotate
"
