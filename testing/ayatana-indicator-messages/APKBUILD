# Contributor: Luca Weiss <luca@z3ntu.xyz>
# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=ayatana-indicator-messages
pkgver=21.12.0
pkgrel=0
pkgdesc="Ayatana Indicator Messages Applet"
url="https://github.com/AyatanaIndicators/ayatana-indicator-messages"
arch="all !s390x !riscv64" # blocked by accountsservice
license="GPL-3.0-only"
makedepends="
	accountsservice-dev
	cmake
	cmake-extras
	dbus-test-runner-dev
	glib-dev
	gtest-dev
	gtk-doc
	intltool
	vala
	"
checkdepends="dbus dbus-test-runner py3-dbus py3-dbusmock"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://github.com/AyatanaIndicators/ayatana-indicator-messages/archive/$pkgver/ayatana-indicator-messages-$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DENABLE_TESTS=ON \
		$CMAKE_CROSSOPTS .
	# Multicore compile broken: https://github.com/AyatanaIndicators/ayatana-indicator-messages/issues/29
	cmake --build build -j1
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7eba8a3ff0e54dea860a019876e025e4def896f004a5f2664979bd9c20d13c94223ddd2ba1bd239e8d1c0d3ff9c93c5e920fb9502e00c94dcde309c8b512f19f  ayatana-indicator-messages-21.12.0.tar.gz
"
